package com.gcl.springcloud.controller;

import com.gcl.springcloud.service.HystrixService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
public class HystrixController {

    @Resource
    private HystrixService hystrixService;

    @Value("${server.port}")
    private String serverPort;


    @GetMapping("/payment/hystrix/ok/{id}")
    public String ok(@PathVariable("id") Integer id ){
        return hystrixService.paymentInfo_ok(id);
    }

    @GetMapping("/payment/hystrix/timout/{id}")
    public String timout(@PathVariable("id") Integer id ){
        return hystrixService.paymentInfo_timout(id);
    }

    //====服务熔断
    @GetMapping("/payment/circuit/{id}")
    public String paymentCircuitBreaker(@PathVariable("id") Integer id)
    {
        String result = hystrixService.paymentCircuitBreaker(id);
        log.info("****result: "+result);
        return result;
    }

}
