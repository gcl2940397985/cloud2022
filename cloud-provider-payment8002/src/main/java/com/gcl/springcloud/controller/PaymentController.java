package com.gcl.springcloud.controller;

import com.gcl.springcloud.entities.CommonResult;
import com.gcl.springcloud.entities.Payment;
import com.gcl.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@Slf4j
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;//添加serverPort

    @Resource
    private PaymentService paymentService;

    @PostMapping( value = "/payment/create" )
    public CommonResult create(@RequestBody Payment payment){
        int result = paymentService.create(payment);
        log.info("****插入的结果是: " + result);
        if ( result > 0 ){
            return new CommonResult( 200 , "插入数据成功,端口为: " + serverPort, result );
        }else {
            return new CommonResult( 444 , "插入数据失败,端口为: "+serverPort );
        }
    }

    @GetMapping( value = "/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id ){
        Payment payment = paymentService.getPaymentById(id);
        if ( payment != null ){
            return new CommonResult( 200, "查询成功,端口为: "+serverPort, payment );
        }else {
            return new CommonResult( 444, ",端口为: "+serverPort+" ,没有ID为："+ id+ " 对应的记录，查询失败");
        }
    }

}
