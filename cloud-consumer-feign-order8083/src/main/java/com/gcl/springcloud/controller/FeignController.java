package com.gcl.springcloud.controller;

import com.gcl.springcloud.entities.CommonResult;
import com.gcl.springcloud.entities.Payment;
import com.gcl.springcloud.service.FeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
public class FeignController {

    @Resource
    private FeignService feignService;

    @GetMapping(value = "/consumer/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id)
    {
        return feignService.getPaymentById(id);
    }

}
