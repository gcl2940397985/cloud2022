package com.gcl.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class OrderMain9083 {

    public static void main(String[] args) {
        SpringApplication.run( OrderMain9083.class , args );
    }

}
