package com.gcl.springcloud.service;


public interface StreamService {

    String send();

}
