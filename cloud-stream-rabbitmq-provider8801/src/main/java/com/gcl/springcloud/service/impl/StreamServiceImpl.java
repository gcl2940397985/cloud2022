package com.gcl.springcloud.service.impl;

import com.gcl.springcloud.service.StreamService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.UUID;

@EnableBinding(Source.class)
@Service
@Slf4j
public class StreamServiceImpl implements StreamService {

    @Resource
    private MessageChannel output;

    @Value("${server.port}")
    private String serverPort;

    @Override
    public String send() {
        String serial = UUID.randomUUID().toString();
        output.send(MessageBuilder.withPayload(serial.getBytes()).build());
        log.info( "****************serial"+ serial + " , server.port : " + serverPort );
        return null;
    }

}
