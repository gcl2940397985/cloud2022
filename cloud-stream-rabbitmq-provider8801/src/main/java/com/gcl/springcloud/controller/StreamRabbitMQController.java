package com.gcl.springcloud.controller;

import com.gcl.springcloud.service.StreamService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class StreamRabbitMQController {

    @Resource
    private StreamService service;

    @GetMapping(value = "/sendMessage")
    public String sendMessage(){
        return service.send();
    }

}

