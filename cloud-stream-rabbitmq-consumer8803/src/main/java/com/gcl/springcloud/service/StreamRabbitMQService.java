package com.gcl.springcloud.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

@Service
@EnableBinding(Sink.class)
public class StreamRabbitMQService {

    @Value("${server.port}")
    private String ServerPort;

    @StreamListener(Sink.INPUT)
    public void get(Message<String> message){
        System.out.println("***************接收到的消息：" + message.getPayload() + " server.port : " + ServerPort );
    }


}
