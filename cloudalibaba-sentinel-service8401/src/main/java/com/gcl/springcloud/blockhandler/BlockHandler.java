package com.gcl.springcloud.blockhandler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.gcl.springcloud.entities.CommonResult;

public class BlockHandler {

    public static CommonResult blockHandlerException1(BlockException exception){
        return new CommonResult(4444, "用户自定义，全局异常处理器1");
    }

    public static CommonResult blockHandlerException2(BlockException exception){
        return new CommonResult(4444, "用户自定义，全局异常处理器2");
    }

}
