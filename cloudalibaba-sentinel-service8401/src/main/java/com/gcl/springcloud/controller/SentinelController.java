package com.gcl.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.gcl.springcloud.blockhandler.BlockHandler;
import com.gcl.springcloud.entities.CommonResult;
import com.gcl.springcloud.entities.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
@Slf4j
public class SentinelController {

    @GetMapping( value = "/testA" )
    public String testA(){
        try{
            TimeUnit.SECONDS.sleep(1);
        }catch (Exception e){
            return "-------testA(error)------";
        }
        log.info( Thread.currentThread().getName() );
        return "-------testA------";
    }

    @GetMapping( value = "/testB" )
    public String testB(){
        return "-------testB------";
    }

    @GetMapping("/testD")
    public String testD() {
        log.info("testD 异常比例");
        int age = 10/0;
        return "------testD";
    }

    @GetMapping("/testHotKey")
    @SentinelResource(value = "testHotKey",blockHandler/*兜底方法*/ = "deal_testHotKey")
    public String testHotKey(@RequestParam(value = "p1",required = false) String p1,
                             @RequestParam(value = "p2",required = false) String p2) {
        //int age = 10/0;
        return "------testHotKey";
    }

    /*兜底方法*/
    public String deal_testHotKey (String p1, String p2, BlockException exception) {
        return "------deal_testHotKey,o(╥﹏╥)o";  //sentinel系统默认的提示：Blocked by Sentinel (flow limiting)
    }


    @GetMapping("/byResource")
    @SentinelResource(value = "byResource",blockHandler = "handleException")
    public CommonResult byResource() {
        return new CommonResult(200,"按资源名称限流测试OK",new Payment(2022L,"serial001"));
    }

    public CommonResult handleException(BlockException exception) {
        return new CommonResult(444,exception.getClass().getCanonicalName()+"\t 服务不可用");
    }

    @GetMapping("/rateLimit/byUrl")
    @SentinelResource(value = "byUrl")
    public CommonResult byUrl()
    {
        return new CommonResult(200,"按url限流测试OK",new Payment(2022L,"serial002"));
    }

    @GetMapping(value = "/consumerBlockHandler")
    @SentinelResource( value = "consumerBlockHandler", blockHandlerClass = BlockHandler.class, blockHandler = "blockHandlerException2")
    public CommonResult comsumerBlockHandler(){
        return new CommonResult(200, "按照用户自定义", new Payment(2022L , "serial003") );
    }

}

