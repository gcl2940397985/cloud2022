package com.gcl.springcloud.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan({"com.gcl.springcloud.dao"})
public class MyBatisConfig {
}
