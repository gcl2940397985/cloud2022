package com.gcl.springcloud.service;

public interface StorageDaoService {
    /**
     * 扣减库存
     */
    void decrease(Long productId, Integer count);
}
