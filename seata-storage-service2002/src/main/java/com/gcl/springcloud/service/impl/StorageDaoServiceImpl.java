package com.gcl.springcloud.service.impl;

import com.gcl.springcloud.dao.StorageDao;
import com.gcl.springcloud.service.StorageDaoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class StorageDaoServiceImpl implements StorageDaoService {
    @Resource
    private StorageDao storageDao;
    @Override
    public void decrease(Long productId, Integer count) {
        log.info("------->storage-service中扣减库存开始");
        storageDao.decrease(productId,count);
        log.info("------->storage-service中扣减库存结束");
    }
}
