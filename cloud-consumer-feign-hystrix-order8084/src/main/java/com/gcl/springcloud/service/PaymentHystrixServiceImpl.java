package com.gcl.springcloud.service;

import org.springframework.stereotype.Service;

@Service
public class PaymentHystrixServiceImpl implements PaymentHystrixService
{
    @Override
    public String paymentInfo_OK(Integer id) {
        return "paymentInfo_OK error";
    }

    @Override
    public String paymentInfo_TimeOut(Integer id) {
        return "paymentInfo_TimeOut error";
    }
}
