package com.gcl.springcloud.service;

import com.gcl.springcloud.domain.Order;

public interface OrderService {

    void create(Order order);

}
