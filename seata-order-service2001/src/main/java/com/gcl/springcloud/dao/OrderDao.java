package com.gcl.springcloud.dao;

import com.gcl.springcloud.domain.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface OrderDao {

    //1 新建订单 返回插入的订单ID
    Long create(Order order);

    //2 修改订单状态，从零改为1
    void update(@Param("orderId") Long orderId, @Param("status") Integer status);
}
