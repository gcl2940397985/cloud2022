package com.gcl.springcloud.controller;

import com.gcl.springcloud.entities.CommonResult;
import com.gcl.springcloud.entities.Payment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class PaymentController {

    @Value(value = "${server.port}")
    private String serverPort;

    public static HashMap<Long, Payment> hashMap = new HashMap<>();

    static {
        hashMap.put(1L, new Payment(1L, "serial1l"));
        hashMap.put(2L, new Payment(2L, "serial2l"));
        hashMap.put(3L, new Payment(3L, "serial3l"));
    }

    @GetMapping(value = "/paymentSQL/{id}")
    public CommonResult<Payment> paymentSQL(@PathVariable("id") Long id){
        Payment payment = hashMap.get(id);
        return new CommonResult<>(200, "From MySQL,ServerPort : " + serverPort, payment);
    }

}
